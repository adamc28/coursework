<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionnaire extends Model
{
    protected $fillable = [
      'title',
      'description',
    ];

    public function questions() {
      return $this->hasMany('App\question');
    }

    public function answer() {
      return $this->hasMany('App\response');
    }
}
