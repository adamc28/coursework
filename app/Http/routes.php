<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/answer');
});



Route::group(['middle' => ['web']], function () {
  Route::auth();

  Route::resource('/answer', 'AnswerController');
  Route::get('/questionnaire/complete', function(){
    return view('answer.complete');
  });
  Route::resource('/home', 'HomeController');
  Route::resource('/admin/create/questionnaire', 'CreateQuestionnaireController');
  Route::resource('/admin/create/question', 'QuestionController');
});
