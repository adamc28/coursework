<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\questionnaire;
use App\question;
use App\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     *
     * returns the home view with all the attributes of the
     * questionnaire model available to it.
     * This enables the view to show things such as questionnaire title.
     */
    public function index()
    {
        $questionnaires = questionnaire::all();
        return view('home')->with('questionnaires', $questionnaires);
    }

    public function create()
    {

        return view('/admin/create/questionnaire');
    }

    /**
     * Gets the questionnaire and checks if it exists
     * Gets all questions that belong to the questionnaire id.
     * Returns view with these values.
     */
    public function show($id)
    {
        $questionnaire = questionnaire::findOrFail($id);
        $questions = question::where('questionnaire_id', $id)->get();
        return view('viewquestionnaire')->with('questionnaire', $questionnaire)->with('questions', $questions);
    }
}
