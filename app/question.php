<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
    protected $fillable = [
      'question',
      'option_1',
      'option_2',
      'option_3',
      'questionnaire_id',
    ];

    public function questionnaire() {
      return $this->belongsTo('App\questionnaire');
    }

}
