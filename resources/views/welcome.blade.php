@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Welcome Survey Pig.
                </div>
                <a class="btn btn-primary" href="{{ url('viewquestionnaire') }}">View Questionnaires</a>
            </div>
        </div>
    </div>
</div>
@endsection
