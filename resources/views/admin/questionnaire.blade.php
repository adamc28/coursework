@extends('layouts.app')

@section('content')
<div class="title">
  <h2>{{ $questionnaire->title }}</h2>
</div>
{!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createquestion')) !!}

    <div class="form-group">
        {!! Form::hidden('questionnaire_link', $questionnaire->id) !!}
        {!! Form::label('question', 'Add Question:') !!}
        {!! Form::text('question', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      {!! Form::label('option_1', 'Add Option:') !!}
      {!! Form::text('option_1', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      {!! Form::label('option_2', 'Add Option:') !!}
      {!! Form::text('option_2', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      {!! Form::label('option_3', 'Add Option:') !!}
      {!! Form::text('option_3', null, ['class' => 'form-control']) !!}
    </div>

    <div>
      {!! Form::submit('Add Question', ['class' => 'btn btn-primary']) !!}
    </div>

{!! Form::close() !!}
<div class="questionlist">
  @if(count($questions) > 0)
    @foreach($questions as $question)
      <h3>{{ $question->question }}</h3>
      <ul>
        <li>{{ $question->option_1 }} - {{ $answers[$question->id]['option_1'] }}</li>
        <li>{{ $question->option_2 }} - {{ $answers[$question->id]['option_2'] }}</li>
        <li>{{ $question->option_3 }} - {{ $answers[$question->id]['option_3'] }}</li>
      </ul>
    @endforeach
  @endif
</div>
@endsection
