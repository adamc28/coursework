<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\questionnaire;
use App\question;
use App\answer;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $questionnaires = questionnaire::all();

      return view('answer.index')->with('questionnaires', $questionnaires);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // fetching all values
        $input = $request->all();
        // gets the keys of the array values
        $quesk = array_keys($request->all());
        $questionnaire = $input['questionnaire_id'];
        /**
        * This will send the answers to the db.
        *
        */
        for($i = 0; $i < count($input); $i++){
          // Getting the current value
          $current = $quesk[$i];
          // Ignore the questionnaire_id
          if($current != 'questionnaire_id'){
            // Ignore the _token value
            if($current != '_token'){
              // Get the values enter them into the db.
              $submit = array(
                'questionnaire_id' => $questionnaire,
                'question_id' => $current,
                'answer' => $input[$current]
              );
              // Create the answer in the db
              answer::create($submit);
            }
          }
        }
        // Send to a complete screen
        return redirect('questionnaire/complete');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questionnaire = questionnaire::findOrFail($id);
        $questions = question::where('questionnaire_id', $id)->get();
        foreach($questions as $question) {
          $options = array();
          $options[1] = $question->option_1;
          $options[2] = $question->option_2;
          if(strlen($question->option_3) > 0){
            $options[3] = $question->option_3;
          }
          $question['options'] = $options;

        }
        return view('answer.show')->with('questionnaire', $questionnaire)->with('questions', $questions);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
