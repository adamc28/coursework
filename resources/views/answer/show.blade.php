@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $questionnaire->title }}</div>
                <div class="panel-body">
                  <section>
                    {!! Form::open(array("action" => 'AnswerController@store')) !!}
                      {!! Form::hidden('questionnaire_id', $questionnaire->id) !!}
                      @foreach($questions as $question)
                        {!! Form::label($question->id, $question->question) !!}
                        <br>
                        {!! Form::select($question->id, $question['options']) !!}
                        <br>
                      @endforeach
                      <br>
                      {!! Form::submit('Submit Answer') !!}
                      {!! Form::close() !!}
                  </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
