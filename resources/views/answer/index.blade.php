@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Questionnaires</div>
                <div class="panel-body">
                  <section>
                    @if (isset ($questionnaires))
                      <ul>
                        @foreach ($questionnaires as $questionnaire)
                          <li><a href="/answer/{{ $questionnaire->id }}">{{ $questionnaire->title }}</a></li>
                        @endforeach
                      </ul>
                    @else
                      <p> no questionnaires added yet </p>
                    @endif
                  </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
