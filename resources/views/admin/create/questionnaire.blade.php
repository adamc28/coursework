@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add Questionnaire</div>

                <div class="panel-body">

                {!! Form::open(array('action' => 'CreateQuestionnaireController@store', 'id' => 'createquestionnaire')) !!}

                    <div class="row large-10 columns">
                        {!! Form::label('title', 'Title:') !!}
                        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
                    </div>

                    <div class="row large-12 columns">
                      {!! Form::label('description', 'Description:') !!}
                      {!! Form::textarea('description', null, ['class' => 'large-8 columns']) !!}
                    </div>

                    <div class="row large-4 columns">
                      {!! Form::submit('Create Questionnaire', ['class' => 'button']) !!}
                    </div>

                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
