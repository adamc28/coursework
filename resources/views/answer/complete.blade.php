@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Thank You!</div>
                <div class="panel-body">
                  <section>
                    <p>Thank you for completing the survey</p>
                  </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
