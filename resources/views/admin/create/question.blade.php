<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Create Questions</title>
  </head>
  <body>
    <h2>{{ $questionnaire->title }}</h2>
    <h3>{{ $questionnaire->description }}</h3>

    @if(session()->has('message.level'))
      <div class="alert alert-{{ session('message.level') }}">
        {!! session('message.content') !!}
      </div>
    @endif
    <h1>Add Questions</h1>


    {!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createquestion')) !!}

        <div class="row large-12 columns">
            {!! Form::hidden('questionnaire_link', $questionnaire->id) !!}
            {!! Form::label('question', 'Add Question:') !!}
            {!! Form::text('question', null, ['class' => 'large-8 columns']) !!}
        </div>

        <div class="row large-12 columns">
          {!! Form::label('option_1', 'Add Option:') !!}
          {!! Form::text('option_1', null, ['class' => 'large-8 columns']) !!}
        </div>

        <div class="row large-12 columns">
          {!! Form::label('option_2', 'Add Option:') !!}
          {!! Form::text('option_2', null, ['class' => 'large-8 columns']) !!}
        </div>

        <div class="row large-12 columns">
          {!! Form::label('option_3', 'Add Option:') !!}
          {!! Form::text('option_3', null, ['class' => 'large-8 columns']) !!}
        </div>

        <div class="row large-4 columns">
          {!! Form::submit('Add Question', ['class' => 'button']) !!}
        </div>

    {!! Form::close() !!}
  </body>
</html>
