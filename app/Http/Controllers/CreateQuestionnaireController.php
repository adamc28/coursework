<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\questionnaire;
use App\question;
use App\Auth;
use App\answer;

class CreateQuestionnaireController extends Controller
{
  /*
   * Secure the set of pages to the admin.
   */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * returns the create questionnaire view with all the attributes of the
     * questionnaire model available to it.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $questionnaires = questionnaire::all();

      return view('/admin/create/questionnaire', ['questionnaire' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/create/questionnaire');
    }

    /**
     * Store a newly created resource in storage.
     * Stores all of the user input from the questionnaire fields
     * Then redirects to a different questionnaire view that has the
     * id appended to it.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $questionnaire = questionnaire::create($request->all());
      //return view('/admin/create/question', ['questionnaire' => $questionnaire]);
      return redirect('/admin/create/questionnaire/' .$questionnaire->id);
    }

    /**
     * Display the specified resource.
     *
     * Gets the questionnaire and checks if it exists
     * Gets all questions that belong to the questionnaire id.
     * Returns view with these values.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $questionnaire = questionnaire::findOrFail($id);
      $questions = question::where('questionnaire_id', $id)->get();
      $answers = array();

        foreach($questions as $question){
          //$answers[$question->id] = [];

          for($i = 1; $i <= 3; $i++){
            $count = answer::where([
              ['question_id', '=', $question->id],
              ['answer', '=', $i]
            ])->count();
            $answers[$question->id]['option_' . $i] = $count;
          }
        }

        return view('admin/questionnaire')->with('questionnaire', $questionnaire)->with('questions', $questions)->with('answers', $answers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
