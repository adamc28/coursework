<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class choice extends Model
{
  protected $fillable = [
    'answer',
    'question_id',
    'questionnaire_id',
  ];

  public function question()
  {
    return $this->belongsTo('App\question');
  }
}
